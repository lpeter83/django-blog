# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('rxublog', '0004_auto_20150712_1717'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='updatetime',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 29, 9, 15, 20, 274000, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='blog',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
