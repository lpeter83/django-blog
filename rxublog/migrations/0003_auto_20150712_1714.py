# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('rxublog', '0002_auto_20150712_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='body',
            field=tinymce.models.HTMLField(),
        ),
    ]
