from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

# Create your views here.
from django.shortcuts import render
from django.template import loader, Context
from django.http import HttpResponse, HttpResponseForbidden
from rxublog.models import Blog
from rxublog.forms import PostForm

# Create your views here.
def archive(request):
    posts = Blog.objects.all()
    paginator = Paginator(posts, 5)
    page_num = request.GET.get('page')
    try:
        posts=paginator.page(page_num)
    except PageNotAnInteger:
        posts=paginator.page(1) #5 post per page
    except EmptyPage:
        posts=paginator.page(paginator.num_pages)

    t = loader.get_template("archive.html")
    c = Context({'posts': posts})
    return HttpResponse(t.render(c))


def post_detail(request, pk):
    post = get_object_or_404(Blog, pk=pk)
    return render(request, 'post_details.html', {'post': post})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('rxublog.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'post_edit.html', {'form': form, 'is_new': True})


def post_edit(request, pk):
    post = get_object_or_404(Blog, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post.save()
            return redirect('rxublog.views.post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'post_edit.html', {'form': form, 'is_new': False})