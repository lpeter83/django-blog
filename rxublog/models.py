from django.utils import timezone
from django.db import models
from django.contrib import admin

# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    updatetime = models.DateTimeField(auto_now=True)


class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'timestamp')


class Meta:
    ordering = ('-timestamp',)